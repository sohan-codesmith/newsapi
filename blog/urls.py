from django.urls import path

from .views import *

from django.urls import path, include
from rest_framework.routers import DefaultRouter
from blog import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register('', views.NewsViewApi, basename='headline')

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', HomeView, name='index'),
    path('api/', include(router.urls)),
    path('phase1/', apiView, name='apiview'),
    path('fetch/data/', fetchApiData, name='fetchdata'),
]

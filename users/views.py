from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.decorators import login_required
from blog.models import Newsapi
import simplejson as json
from users.models import Profile


# Create your views here.

def register(request):
    if request.method == 'POST':
        # this is post
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account is created for{username}!,Now you can LogIn')
            return redirect('login')
    else:
        # this is get request
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


@login_required()
def profile(request):
    prof_obj = Profile.objects.get(user=request.user)
    country = prof_obj.preferred_country
    jsonDec = json.decoder.JSONDecoder()
    news_source = []
    if prof_obj.preferred_source:
        news_source = jsonDec.decode(prof_obj.preferred_source)

    source = ''
    for l in news_source:
        source = source + l + ','

    if request.method == 'POST':
        preferred_source = request.POST.getlist('news_source')
        print(preferred_source)

        preferred_country = request.POST.get('country')
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES,
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            profile = p_form.save(commit=False)
            if len(preferred_source) > 0:
                profile.preferred_source = json.dumps(preferred_source)
            else:
                profile.preferred_source = json.dumps(news_source)
            profile.preferred_country = preferred_country
            profile.save()
            messages.success(request, f'Your account has been successfully updated')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    newsapi = Newsapi.objects.all()
    unique_country = Newsapi.objects.values('country').distinct()
    context = {
        'u_form': u_form,
        'p_form': p_form,
        'newsapi': newsapi,
        'unique_country': unique_country,
        'source': source,
        'country': country,
    }

    return render(request, 'users/profile.html', context)
